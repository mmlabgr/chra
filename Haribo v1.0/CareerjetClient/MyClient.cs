﻿using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CareerjetClient
{
    public class MyClient
    {
        
        public MyClient()
        {

        }

        public string Search(Hashtable cargs, string countryInit)
        {
            WebService.Careerjet.Client c = new WebService.Careerjet.Client(countryInit);
            JObject res = c.Search(cargs);
            string json = JsonConvert.SerializeObject(res, Formatting.None);
            return json;
        }

    }
    
}
