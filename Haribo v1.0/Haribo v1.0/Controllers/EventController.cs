﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Linq;
using Haribo_v1._0.Models;
using Newtonsoft.Json;
using Microsoft.Bot.Connector;

namespace Haribo_v1._0.Controllers
{
    public class EventController : ApiController
    {
        [HttpPost]
        [Route("api/push")]
        public async Task<HttpResponseMessage> SendMessage()
        {

            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                await SendProactiveMessage();
            }
            catch (Exception ex)
            {
                resp = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            return resp;
        }

        private async Task SendProactiveMessage()
        {
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            List<UserLog> userLogs = new List<UserLog>();
            userLogs = (from u in userDb.UserLog where u.status == "running" && u.convLog != null select u).ToList();

            foreach (var user in userLogs)
            {
                try
                {
                    Rootobject1 logs = JsonConvert.DeserializeObject<Rootobject1>(user.convLog);
                    var userAccount = new ChannelAccount(logs.user.id, logs.user.name);
                    var botAccount = new ChannelAccount(logs.bot.id, logs.bot.name);
                    MicrosoftAppCredentials.TrustServiceUrl(logs.serviceUrl);
                    var connector = new ConnectorClient(new Uri(logs.serviceUrl), ConfigurationManager.AppSettings["ΧΧΧΧΧΧΧΧΧΧ"], ConfigurationManager.AppSettings["ΧΧΧΧΧΧΧΧΧΧ"]);
                    var newMessage = Activity.CreateMessageActivity();
                    newMessage.Type = ActivityTypes.Message;
                    newMessage.From = botAccount;
                    newMessage.Recipient = userAccount;
                    newMessage.Text = "Hello, I have the available jobs for this week! ";

                    newMessage.TextFormat = TextFormatTypes.Plain;
                    newMessage.SuggestedActions = new SuggestedActions()
                    {
                        Actions = new List<CardAction>()
                        {
                            new CardAction(){ Title = "Ok", Type=ActionTypes.PostBack, Value="Retry" }
                        }
                    };

                    newMessage.ChannelId = logs.channelId;
                    var conversation = await connector.Conversations.CreateDirectConversationAsync(botAccount, userAccount);
                    newMessage.Conversation = new ConversationAccount(id: conversation.Id);
                    await connector.Conversations.SendToConversationAsync((Activity)newMessage);
                }
                catch (Exception)
                {
                }

            }


        }

    }
}
