﻿
namespace Haribo_v1._0
{
    public class Rootobject
    {
        public Job[] jobs { get; set; }
    }

    public class Job
    {
        public string salary_min { get; set; }
        public string locations { get; set; }
        public string salary_type { get; set; }
        public string date { get; set; }
        public string description { get; set; }
        public string salary_currency_code { get; set; }
        public string salary { get; set; }
        public string site { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string salary_max { get; set; }
        public string company { get; set; }
    }
}