﻿using System;
using Microsoft.Bot.Builder.FormFlow;


namespace Haribo_v1._0.Forms
{
    [Serializable]
    public class UpdateCompanyName
    {
        [Optional]
        [Prompt("If you have a specific company in mind, let me know! (If not, type 'No')")]
        public string CompanyName { get; set; }


        public static IForm<UpdateCompanyName> BuildForm()
        {

            return new FormBuilder<UpdateCompanyName>()
                .AddRemainingFields()
                .Confirm("Company Name: {CompanyName}\r\rIs that correct? (y | n)")
                .Build();
        }

    }
}