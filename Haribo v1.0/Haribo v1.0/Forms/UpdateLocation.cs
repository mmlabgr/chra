﻿using System;
using Microsoft.Bot.Builder.FormFlow;

namespace Haribo_v1._0.Forms
{

    [Serializable]
    public class UpdateLocation
    {
        [Prompt("Please select your {&} of preference? {||}")]
        public CountryOp? Country;
        [Prompt("Do you want to search for job openings in the whole country or specific city/state? {||}")]
        public ChooseLocationOp? ChooseLocation;
        [Prompt("Type a {&} or state as your job location?")]
        public string City;



        public static IForm<UpdateLocation> BuildForm()
        {

            return new FormBuilder<UpdateLocation>()
               
                .Field(nameof(Country))
                .Field(nameof(ChooseLocation))
                .Field(nameof(City), IsOther ) //displays option city only if user selects "Choose City"
                .Confirm("Job Location: {?{City},} {Country}\r\rIs that correct? (y | n)")
                .Build();
        }

       private static bool IsOther (UpdateLocation state)
        {
            return state.ChooseLocation == ChooseLocationOp.ChooseCity;
        }

    }
}