﻿using System;
using Microsoft.Bot.Builder.FormFlow;


namespace Haribo_v1._0.Forms
{
    [Serializable]
    public class UpdateHardSkills
    {
        [Prompt("Please type the {&} you want to add")]
        public string HardSkills { get; set; }


        public static IForm<UpdateHardSkills> BuildForm()
        {

            return new FormBuilder<UpdateHardSkills>()
                .AddRemainingFields()
                .Confirm("Hard Skills: {HardSkills}\r\rIs that correct? (y | n)")
                .Build();
        }
    }
}