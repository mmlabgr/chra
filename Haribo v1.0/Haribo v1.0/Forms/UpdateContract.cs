﻿using System;
using Microsoft.Bot.Builder.FormFlow;


namespace Haribo_v1._0.Forms
{
    [Serializable]
    public class UpdateContract
    {
        [Prompt("Select your prefered {&} ! {||}")]
        public ContractPeriodOp? ContractPeriod;
        [Prompt("Select your prefered {&} ! {||}")]
        public ContractTypeOp? ContractType;

        public static IForm<UpdateContract> BuildForm()
        {

            return new FormBuilder<UpdateContract>()
                .Build();
        }


    }
}