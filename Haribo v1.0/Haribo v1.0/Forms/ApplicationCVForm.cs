﻿using System;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;


namespace Haribo_v1._0.Forms
{
    
    public enum ContractPeriodOp
    {
        Fulltime,
        Partime,
        All
    }

    public enum ContractTypeOp
    {
        Permanent,
        Contract,
        Temporary,
        Training,
        Voluntary,
        All
    }

    public enum CountryOp
    {
        
        Greece,
        Netherlands,
        UnitedKingdom
    }

    public enum ChooseLocationOp
    {
        WholeCountry,
        ChooseCity
    }

    public enum ChooseSkillOp
    {
        HardSkills,
        SoftSkills
     }

    [Serializable]
    //[Template(TemplateUsage.NoPreference, "No Specific Preference")]
    public class ApplicationCVForm
    {
        public int flag = 0; 
        [Prompt("Please type your {&} based on your occupation and/or the job position you have in mind. \n\n (e.g for a programmers hard skills are: C++,Java,Linux,foreign languages,etc) \n\n p.s : please seperate skills by commas")]
        public string HardSkills { get; set; }
        [Optional]
        [Prompt("Please type up to 4 {&} you consider your assets. (e.g patience,leadership,creativity,etc) \n\n [This question is optional, type 'No' if you don't want to answer]")]
        public string SoftSkills { get; set; }
        [Prompt("Choose which set of skills you want to complete first. {||}")]
        public ChooseSkillOp? ChooseSkill;
        [Optional]
        [Prompt("Have you a job in mind? Please type the title. (If you haven't thought of it, no worries! Just type 'No') ")]
        public string JobTitle { get; set; }
        [Optional]
        [Prompt("If you have a specific company in mind, let me know! (If not, type 'No')")]
        public string CompanyName { get; set; }
        [Prompt("Please select your {&} of preference? {||}")]
        public CountryOp? Country;
        [Prompt("Do you want to search for job openings in the whole country or specific city/state? {||}")]
        public ChooseLocationOp? ChooseLocation;
        [Prompt("Type a {&} or state as your job location?")]
        public string City;
        [Prompt("Select your prefered {&} ! {||}")]
        public ContractPeriodOp? ContractPeriod;
        [Prompt("Select your prefered {&} ! {||}")]
        public ContractTypeOp? ContractType;

       


        public static IForm<ApplicationCVForm> BuildForm()
        {
            return new FormBuilder<ApplicationCVForm>()
                .Message("\U0001F60E")
                .Message("Let's try to make a personalized profile for you! (You can type \"quit\" at any point to exit) \r\r (P.S If you make a mistake at any point, don't worry! You can fix it at the end!)")
                .Field(nameof(JobTitle))
                .Field(nameof(CompanyName))
                .Field(new FieldReflector<ApplicationCVForm>(nameof(ChooseSkill))
                    .SetActive(state=>true)
                    .SetNext((value, state) =>
                    {
                        var selection = (ChooseSkillOp) value;

                        if (selection == ChooseSkillOp.HardSkills)
                        {
                            return new NextStep(new []{nameof(HardSkills)});
                        }
                        
                            return  new NextStep(new []{nameof(SoftSkills)});
                     })
                )
                .Field(new FieldReflector<ApplicationCVForm>(nameof(HardSkills))
                    .SetActive(state=>state.ChooseSkill.HasValue)
                    .SetNext((value, state) =>
                    {
                        if (state.ChooseSkill==ChooseSkillOp.SoftSkills)
                        {
                            state.flag = 1;  //avoid loop
                            return new NextStep();
                        }
                        return new NextStep(new []{nameof(SoftSkills)});
                    })
                )
                .Field(new FieldReflector<ApplicationCVForm>(nameof(SoftSkills))
                    .SetActive(state => state.flag.Equals(0) && state.ChooseSkill.HasValue)
                    .SetNext((value, state) =>
                    {
                        if (state.ChooseSkill==ChooseSkillOp.HardSkills)
                        {
                            return new NextStep(); 
                        }
                        return  new NextStep(new []{nameof(HardSkills)});
                    })
                )

                .Field(nameof(Country))
                .Field(nameof(ChooseLocation))
                .Field(nameof(City), IsOther) //displays option city only if user selects "Choose City"
                .Field(nameof(ContractPeriod))
                .Field(nameof(ContractType))
                .Confirm("Great. I have the following details and I am ready to submit your CV.\r\r Job Title: {JobTitle}\r\r Company Name: {CompanyName}\r\r Your Skills:\r\r \u002D Hard Skills: {HardSkills}\r\r \u002D Soft Skills: {SoftSkills}\r\rJob Location: {?{City},} {Country}\r\rContract Period: {ContractPeriod}\r\rType of Contract: {ContractType}\r\rIs that correct? (yes | no)")
                .Build();
        }
        
        private static bool IsOther(ApplicationCVForm state)
        {
            return state.ChooseLocation == ChooseLocationOp.ChooseCity;
        }
    }
}