﻿using System;
using Microsoft.Bot.Builder.FormFlow;


namespace Haribo_v1._0.Forms
{

    [Serializable]
   
    public class UpdateJobTitle
    {
        
        [Optional]
        [Prompt("Have you a job in mind? Please type the title. (If you haven't thought of it, no worries! Just type 'No') ")]
        public string JobTitle { get; set; }
       

        public static IForm<UpdateJobTitle> BuildForm()
        {

            return new FormBuilder<UpdateJobTitle>()
                .AddRemainingFields()
                .Confirm("Job Title: {JobTitle}\r\rIs that correct? (y | n)")
                .Build();
        }
    }
}