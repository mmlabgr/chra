﻿
namespace Haribo_v1._0
{
    public class Rootobject1
    {
        public User user { get; set; }
        public Bot bot { get; set; }
        public conversation conversation { get; set; }
        public string channelId { get; set; }
        public string serviceUrl { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Bot
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class conversation
    {
        public string id { get; set; }
    }


}