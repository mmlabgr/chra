﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Haribo_v1._0.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Threading;

namespace Haribo_v1._0.Dialogs
{
    using Properties;
    using Extensions;

    [Serializable]
    public class ReturnDialog : IDialog<object>
    {
        [NonSerialized]
        Timer t;

        private int _flag = 0;
        private readonly int _userId;

        public ReturnDialog(int userid)
        {
            _userId = userid;
        }

        public async Task StartAsync(IDialogContext context)
        {
             PromptDialog.Choice(context, AfterSelectOption, new string[] { Resources.Positive, Resources.Negative }, "I searched and here is what I could find according to your cv ! Want to proceed?");
        }

        private async Task AfterSelectOption(IDialogContext context, IAwaitable<string> result)
        {
            var reply = await result;
            if (reply == Resources.Negative)
            {
                await context.PostAsync(Resources.ThankYou_Message + "\U0001F600");
                context.Done(String.Empty);
            }
            else
            {

                HaRiBoDbEntities userDb = new HaRiBoDbEntities();
                DateTime currDateTime = DateTime.UtcNow;
                var lastRec = userDb.JobLog.OrderByDescending(o => o.joblog_id).First();
                var dbDateTime = lastRec.update_date;
                var diff = (currDateTime - dbDateTime);
                var totalHours = (int)Math.Abs(diff.TotalHours);

                if (totalHours < 10)
                {
                    //return result from existing db
                    await ReturnResult(context);
                    await context.PostAsync(Resources.Notification_Message);
                    await context.PostAsync(Resources.ThankYou_Message + "\U0001F600");
                    context.Done(String.Empty);
                }
                else
                {
                    //update database
                    t = new Timer(TimerEvent);
                    t.Change(5000, Timeout.Infinite);
                    await context.PostAsync("Let me update my database with the newest job positions in the market and I will be back in a couple of minutes");
                    context.Wait(MessageReceivedAsync);
                    context.Done(String.Empty);
                }
            }
        }

        public async Task ReturnResult(IDialogContext context)
        {
            List<JobLog> availableJobs = new List<JobLog>();
            List<JobLog> returnedJobs = new List<JobLog>();
            List<JobLog> returnedComplexJobs = new List<JobLog>();
            StringComparison comp = StringComparison.OrdinalIgnoreCase;
            int limit = 2000;
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();

            //all the info we got from the user 
            var user = (from u in userDb.UserLog where u.user_id == _userId select u).First();
            var skills = (from s in userDb.UserSkill where s.user_id == _userId select s.skill_id).ToArray();
            string[] skillName = new string[skills.Length];
            for (int i = 0; i < skills.Length; i++)
            {
                var tmp = skills[i];
                skillName[i] = (from s in userDb.Skills where s.skill_id == tmp select s.skill_type).First();
            }
            var jobTitle = (from j in userDb.JobTitle where j.job_id == user.job_id select j.job_name).First();
            var compName = (from c in userDb.CompanyTitle where c.comp_id == user.comp_id select c.comp_name).First();
            var location = (from l in userDb.Location where l.user_id == user.user_id select l).First();
            var city = location.city_name;
            if (city == null)
            {
                availableJobs = (from aj in userDb.JobLog where aj.country_id == location.country_id select aj).ToList();
                availableJobs = availableJobs.OrderByDescending(x => x.update_date).Take(limit).ToList(); //has 2days job offers for 1 country 
            }
            else
            {
                availableJobs = (from aj in userDb.JobLog where aj.country_id == location.country_id && aj.location_city == city select aj).ToList();
                availableJobs = availableJobs.OrderByDescending(x => x.update_date).Take(limit).ToList(); //has 2days job offers for 1 country 
            }

            //compare with database
            //1-------------------------------------------------------------------------------
            if (jobTitle == null && compName == null && _flag == 0)
            {
                //only skills
                foreach (var avLog in availableJobs)
                {
                    for (int i = 0; i < skills.Length; i++)
                    {
                        var findsimilar = avLog.description.Contains(skillName[i], comp);
                        if (findsimilar)
                        {
                            returnedJobs.Add(avLog);
                        }
                    }
                }
                bool isEmpty = !returnedJobs.Any();
                if (isEmpty)
                {
                    await context.PostAsync("Sorry there is no job available at the moment that would be relevant to your CV... Please try again later ...");
                }
                else
                {
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }
                _flag = 1;
            }

            //2-----------------------------------------------------------------------
            if (jobTitle != null && compName != null && _flag == 0)
            {
                //only jobtitle and compname
                //sort first by company then by jobtitle
                //if comp = null  then only jobtitles
                foreach (var avLog in availableJobs)
                {
                    var findsimilarcomp = avLog.company.Contains(compName, comp);
                    if (findsimilarcomp)
                    {
                        var findsimilartitlte = avLog.title.Contains(jobTitle, comp);
                        if (findsimilartitlte)
                        {
                            returnedComplexJobs.Add(avLog);
                        }
                    }
                    else
                    {
                        var findsimilartitlte = avLog.title.Contains(jobTitle, comp);
                        if (findsimilartitlte)
                        {
                            returnedJobs.Add(avLog);
                        }
                    }
                }
                bool isEmptyComplex = !returnedComplexJobs.Any();
                bool isEmpty = !returnedJobs.Any();
                if ((isEmptyComplex && isEmpty) || (!isEmptyComplex && !isEmpty))
                {
                    await context.PostAsync("Sorry there is no job available at the moment that would be relevant to your CV... Please try again later ...");
                }
                else if (!isEmptyComplex)
                {
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedComplexJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }
                else
                {
                    await context.PostAsync("Sorry there is no job available in the company you want but here are some according to the job title you gave me:");
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }

                _flag = 1;
            }
            //3---------------------------------------------------------------------
            if (jobTitle != null && _flag == 0)
            {
                foreach (var avLog in availableJobs)
                {
                    var findsimilar = avLog.title.Contains(jobTitle, comp);
                    if (findsimilar)
                    {
                        returnedJobs.Add(avLog);
                    }
                }

                bool isEmpty = !returnedJobs.Any();
                if (isEmpty)
                {
                    await context.PostAsync("Sorry there is no job available at the moment that would be relevant to your CV... Please try again later ...");
                }
                else
                {
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }
                _flag = 1;
            }
            //4-----------------------------------------------------------------------
            if (compName != null && _flag == 0)
            {
                //only company and skills 
                //sort by company then skills
                foreach (var avLog in availableJobs)
                {
                    var findsimilarcomp = avLog.company.Contains(compName, comp);
                    if (findsimilarcomp)
                    {
                        for (int i = 0; i < skills.Length; i++)
                        {
                            var findsimilar = avLog.description.Contains(skillName[i], comp);
                            if (findsimilar)
                            {
                                returnedComplexJobs.Add(avLog);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < skills.Length; i++)
                        {
                            var findsimilar = avLog.description.Contains(skillName[i], comp);
                            if (findsimilar)
                            {
                                returnedJobs.Add(avLog);
                            }
                        }
                    }
                }
                bool isEmptyComplex = !returnedComplexJobs.Any();
                bool isEmpty = !returnedJobs.Any();
                if ((isEmptyComplex && isEmpty) || (!isEmptyComplex && !isEmpty))
                {
                    await context.PostAsync("Sorry there is no job available at the moment that would be relevant to your CV... Please try again later ...");
                }
                else if (!isEmptyComplex)
                {
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedComplexJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }
                else
                {
                    await context.PostAsync("Sorry there is no job available in the company you want but here are some according to your skills:");
                    var reply = context.MakeMessage();
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    IList<Attachment> GetCardsAttachments = new List<Attachment>();
                    foreach (var log in returnedJobs.Take(10))
                    {

                        Attachment joblist = GetHeroCard(
                            log.title,
                            log.location_city,
                            log.description,
                            new CardAction(ActionTypes.OpenUrl, Resources.More_Info, value: log.url));

                        GetCardsAttachments.Add(joblist);
                    }
                    reply.Attachments = GetCardsAttachments;
                    await context.PostAsync(reply);
                }
                _flag = 1;
            }

        }

        private static Attachment GetHeroCard(string title, string subtitle, string text, CardAction cardAction)
        {
            var heroCard = new HeroCard
            {
                Title = title,
                Subtitle = subtitle,
                Text = text,
                Buttons = new List<CardAction>() { cardAction },
            };

            return heroCard.ToAttachment();
        }

        public void TimerEvent(object target)
        {
            t.Dispose();
            SendUpdateDb.ResumeUpdate(_userId);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            await ReturnResult(context);
            await context.PostAsync(Resources.Notification_Message);
            await context.PostAsync("\U0001F600" + Resources.ThankYou_Message);
            context.Done(String.Empty);
        }

    }

}