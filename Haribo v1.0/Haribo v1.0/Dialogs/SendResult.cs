﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Haribo_v1._0.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace Haribo_v1._0.Dialogs
{
    public class SendResult
    {
        
        public static async Task Resume(int userid)
        {
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            var message = JsonConvert.DeserializeObject<ConversationReference>((from c in userDb.UserLog where c.user_id == userid select c.convLog).First()).GetPostToBotMessage();
            MicrosoftAppCredentials.TrustServiceUrl(message.ServiceUrl);
            //var client = new ConnectorClient(new Uri(message.ServiceUrl));

            using (var scope = DialogModule.BeginLifetimeScope(Conversation.Container, message))
            {
                var botData = scope.Resolve<IBotData>();
                await botData.LoadAsync(CancellationToken.None);

                //This is our dialog stack
                var task = scope.Resolve<IDialogTask>();

                //interrupt the stack. This means that we're stopping whatever conversation that is currently happening with the user
                //Then adding this stack to run and once it's finished, we will be back to the original conversation
                var dialog = new ReturnDialog(userid);
                task.Call(dialog.Void<object, IMessageActivity>(), null);

                await task.PollAsync(CancellationToken.None);

                //flush dialog stack
                await botData.FlushAsync(CancellationToken.None);

            }
        }
        
    }
}