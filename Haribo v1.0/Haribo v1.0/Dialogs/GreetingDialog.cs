﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Haribo_v1._0.Models;
using Microsoft.Bot.Builder.ConnectorEx;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;


namespace Haribo_v1._0.Dialogs
{
    using Properties;
    using Extensions;

    [Serializable]
    public class GreetingDialog : IDialog
    {
        public static int Count = 0;
        public int userid;
        public string[] fullName;
        public string UserName = String.Empty;
        TextInfo myName = new CultureInfo("en-US", false).TextInfo; //use for ToTitleCase

        public GreetingDialog(int userId)
        {
            userid = userId;
        }

        public async Task StartAsync(IDialogContext context)
        {
            
            if (Count >= 1 && userid != 0)
            {
                context.UserData.TryGetValue<string>("Name", out UserName);
                await context.PostAsync(String.Format("Welcome {0} !", myName.ToTitleCase(UserName))); //ToTitleCase : Converts to uppercase if space is detedcted
                context.Done(1);
            }
            else
            {
                if (Count >= 1 && userid == 0)
                {
                    Count = 0;
                    UserName = String.Empty;
                    context.UserData.Clear();
                }
                await WelcomeMessageAsync(context);

            }

        }

        private async Task Respond(IDialogContext context)
        {
            context.UserData.TryGetValue<string>("Name", out UserName);

            if (string.IsNullOrEmpty(UserName) && Count == 0)
            {

                await context.PostAsync("Please type your name !");
                context.UserData.SetValue<bool>("GetName", true);
            }
            else
            {


                await context.PostAsync(String.Format("Welcome {0} !", myName.ToTitleCase(UserName)));
                Count++;
            }
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var conversationReference = message.ToConversationReference();

            var GetName = false;

            context.UserData.TryGetValue<string>("Name", out UserName);
            context.UserData.TryGetValue<bool>("GetName", out GetName);

            if (GetName)
            {
                UserName = message.Text;
                context.UserData.SetValue<string>("Name", UserName);
                context.UserData.SetValue<bool>("GetName", false);

            }

            await Respond(context);

            //--------saves in database
            fullName = myName.ToTitleCase(UserName).Split(' ').Select(nValue => nValue.Trim()).ToArray();
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            UserLog dbUser = new UserLog();
            dbUser.convLog = JsonConvert.SerializeObject(conversationReference); 
            dbUser.Name = fullName[0];
            if (fullName.Length > 1)
            {
                dbUser.Surname = fullName[1];
            }
            else
            {
                dbUser.Surname = null;
            }
            dbUser.status = "running";
            userDb.UserLog.Add(dbUser);
            userDb.SaveChanges();
            userid = dbUser.user_id;
            //-----------------------------------
            context.Done(userid); //returns userid in callback 


        }

        //HeroCard with Welcome Message
        private async Task WelcomeMessageAsync(IDialogContext context)
        {
            var reply = context.MakeMessage();

            var options = new[]
            {
                Resources.Welcome_Button,
                Resources.Exit_Button
            };
            reply.AddHeroCard(
                String.Format(Resources.Welcome_Message_Title),
                Resources.Welcome_Message_Subtitle,
                options,
                new[] { "https://image.ibb.co/kLUrx8/haribot_big.png" });

            await context.PostAsync(reply);

            context.Wait(OnOptionSelected);
        }

        //When button in HeroCard is selected
        private async Task OnOptionSelected(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var reply = await result;

            if (reply.Text == Resources.Welcome_Button)
            {
                //Asks for name
                await Respond(context);
                if (Count >= 1) //added in order not to loop if you start a new conversation
                {
                    context.Done(1);
                }
                else
                {
                    context.Wait(MessageReceivedAsync);
                }

            }
            else if (reply.Text == Resources.Exit_Button)
            {
                //Exits
                await context.PostAsync("\U0001F600" + Resources.ThankYou_Message);
                var flag = 0;
                context.Done(flag);
            }
        }

    }
}