﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Haribo_v1._0.Forms;
using Haribo_v1._0.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Timer = System.Threading.Timer;


namespace Haribo_v1._0.Dialogs
{
    using Properties;

    [LuisModel("ΧΧΧΧΧΧΧΧΧΧ", "ΧΧΧΧΧΧΧΧΧΧ", domain: "westus.api.cognitive.microsoft.com")]
    [Serializable]

    public class LUISDialog : LuisDialog<object>
    {
        [NonSerialized]
        Timer t; 
        public int userid = 0;
        private bool flag_cv = false;
        private ApplicationCVForm searchQuery;

        //All LuisIntents
        [LuisIntent("")]

        public async Task None(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("I'm sorry, I don't know what you mean! \n To start: type 'hi'\n To exit: type 'exit/thanks'");
            context.Wait(MessageReceived);
        }


        [LuisIntent("Help")]

        public async Task HelpMenu(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(String.Format("Welcome to the help menu. Here are some commads you can type: {0} 'Restart' : Start the interview from the start {0} 'Exit' : Quit the interview",Environment.NewLine));
            context.Wait(MessageReceived);
        }


        [LuisIntent("Greeting")]

        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            context.Call(new GreetingDialog(userid), Callback);
        }

        private async Task Callback(IDialogContext context, IAwaitable<object> result)
        {
            var message = await result;
            var tmp = (int) message;
            if (tmp == 0)
            {
                context.Wait(MessageReceived);
            }
            else
            {
                if (userid == 0)
                {
                    userid = tmp;
                }

                var reply = context.MakeMessage();
                reply.Text = ("Have you made a CV with me already?");
                reply.SuggestedActions = new SuggestedActions()
                {

                    Actions = new List<CardAction>()
                    {
                        new CardAction() {Title = "Yes", Type = ActionTypes.PostBack, Value = "olduser"},
                        new CardAction() {Title = "No", Type = ActionTypes.ImBack, Value = "No I have not!"}


                    }
                };

                await context.PostAsync(reply);
                context.Wait(MessageReceived); //starts the chain all over again , waits for message from the button
            }

        }


        [LuisIntent("Farewell")]

        public async Task Farewell(IDialogContext context, LuisResult result)
        {
            await context.PostAsync(Resources.ThankYou_Message + "\U0001F600");
            context.Wait(MessageReceived);
        }


        [LuisIntent("OldUser")]

        public async Task ReturnUser(IDialogContext context, LuisResult result)
        {
            var reply = context.MakeMessage();
            reply.Text= ("Perfect! What kind of change would you like to make in your CV?");
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title = "Update CV", Type = ActionTypes.PostBack, Value = "Update"},
                    new CardAction() {Title = "Delete CV", Type = ActionTypes.PostBack, Value = "Delete"},
                    new CardAction() {Title = "Retry job search", Type = ActionTypes.PostBack, Value = "Retry"},
                    new CardAction() {Title = "Change Status", Type = ActionTypes.PostBack, Value = "Change status"},
                    new CardAction() {Title = "Exit", Type = ActionTypes.PostBack, Value = "exit"}


                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived); //starts the chain all over again , waits for message from the button
        }

        
        [LuisIntent("UpdateUser")] //delete current and run form again 
        
        public async Task UpdateDb(IDialogContext context, LuisResult result)
        {

            var reply = context.MakeMessage();
            reply.Text = "What would you like to change?";
            reply.SuggestedActions = new SuggestedActions
            {

                Actions = new List<CardAction>
                {
                    new CardAction() {Title = String.Format("Job Title: {0}" , searchQuery.JobTitle), Type = ActionTypes.PostBack, Value = "JobTitle"},
                    new CardAction() {Title = String.Format("Company Name: {0}" , searchQuery.CompanyName), Type = ActionTypes.PostBack, Value = "CompanyName"},
                    new CardAction() {Title = String.Format("City: {0} , Country: {1}" ,searchQuery.City,searchQuery.Country), Type = ActionTypes.PostBack, Value = "Location"},
                    new CardAction() {Title = String.Format("Hard Skills: {0}" ,searchQuery.HardSkills), Type = ActionTypes.PostBack, Value = "HardSkills"},
                    new CardAction() {Title = String.Format("Contract Period: {0} , Contract Type: {1}" ,searchQuery.ContractPeriod.ToString(),searchQuery.ContractType.ToString()), Type = ActionTypes.PostBack, Value = "Contract"},
                    new CardAction() {Title = "Exit", Type = ActionTypes.PostBack, Value = "Exit"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(UpdateOption);
            
        }
        
        private async Task UpdateOption(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var option = await result;

            switch (option.Text)
            {
                case "JobTitle":
                    var updatejForm = new FormDialog<UpdateJobTitle>(new UpdateJobTitle(), UpdateJobTitle.BuildForm, FormOptions.PromptInStart);
                    context.Call(updatejForm, AfterJobUpdateForm);
                    break;
                case "CompanyName":
                    var updatecForm = new FormDialog<UpdateCompanyName>(new UpdateCompanyName(), UpdateCompanyName.BuildForm, FormOptions.PromptInStart);
                    context.Call(updatecForm, AfterCompUpdateForm);
                    break;
                case "Location":
                    var updatelForm = new FormDialog<UpdateLocation>(new UpdateLocation(), UpdateLocation.BuildForm, FormOptions.PromptInStart);
                    context.Call(updatelForm, AfterLocUpdateForm);
                    break;
               case "HardSkills":
                   var updatehForm = new FormDialog<UpdateHardSkills>(new UpdateHardSkills(), UpdateHardSkills.BuildForm, FormOptions.PromptInStart);
                   context.Call(updatehForm, AfterHardSUpdateForm);
                    break;
                case "Contract":
                    var updatecoForm = new FormDialog<UpdateContract>(new UpdateContract(), UpdateContract.BuildForm, FormOptions.PromptInStart);
                    context.Call(updatecoForm, AfterConUpdateForm);
                    break;
                case "Exit":
                    t = new Timer(TimerEvent);
                    t.Change(5000, Timeout.Infinite);
                    await context.PostAsync("Let me check if there are any new job openings according the new info you provided");
                    context.Wait(MessageReceived);
                    break;
            }
           
        }

        
        [LuisIntent("DeleteUser")]

        public async Task DeleteDb(IDialogContext context, LuisResult result)
        {
           HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            
            var userToDelete = (from a in userDb.UserLog where a.user_id == userid select a).First();
            userDb.UserLog.Remove(userToDelete);
            userDb.SaveChanges();
            userid = 0; //returns user id to original value
            await context.PostAsync("I have deleted your CV. Thank you for using our service");
            context.Wait(MessageReceived);
        }


        [LuisIntent("RetrySearch")]

        public async Task RetryDbSearch(IDialogContext context, LuisResult result)
        {
            t = new Timer(TimerEvent);
            t.Change(5000, Timeout.Infinite); 
            await context.PostAsync("Let's see..");
            context.Wait(MessageReceived);
        }

        [LuisIntent("ChangeStatus")]
        public async Task ChangeStatus(IDialogContext context, LuisResult result)
        {
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            var user = (from u in userDb.UserLog where u.user_id == userid select u).First();
            user.status = (user.status == "running") ? "completed" : "running";
            userDb.SaveChanges();
            await context.PostAsync(String.Format("Perfect! Your status has now changed to '{0}'" , user.status));
            context.Wait(MessageReceived);
        }


        [LuisIntent("ApplicationCV")]

        public async Task ApplicationCv(IDialogContext context, LuisResult result)
        {
            if (flag_cv)
            {
                context.PostAsync("You have already created a cv form. If you want to create a new one you have to delete the previous one! Let's try again!");
                context.Call(new GreetingDialog(userid), Callback);
            }
            else
            {
                var enrollmentForm = new FormDialog<ApplicationCVForm>(new ApplicationCVForm(), ApplicationCVForm.BuildForm, FormOptions.PromptInStart);
                context.Call(enrollmentForm, AfterCompletionForm);
            }
            
        }
        

        //AfterCompletion of the Application and Update Forms
        private async Task AfterCompletionForm(IDialogContext context, IAwaitable<ApplicationCVForm> result)
        {
            string fcity = null;
            flag_cv = true;
            searchQuery = await result;
            var skill = searchQuery.HardSkills;
            string[] fskills = skill.Split(',').Select(sValue => sValue.Trim()).ToArray(); //seperates skills by commas and trims whitespaces. saves array Skills[i]
            var fJobtitle = searchQuery.JobTitle; //saves jobtitle (optional)
            if (fJobtitle!=null)
            {
                fJobtitle = fJobtitle.ToLower();
            }
            var fcompName = searchQuery.CompanyName; //saves company name (optional)
            if (fcompName!=null)
            {
                fcompName = fcompName.ToLower();
            }
            var fcountry = searchQuery.Country.ToString(); //saves the country the user has selected
            var flocation = searchQuery.ChooseLocation.ToString();
            var conperiod = searchQuery.ContractPeriod.ToString().ToLower(); //saves the enum as a string in lowercase - make a function where it will save the first letter or "all" as a string
            string fcontractp = SaveFirstLetter(conperiod); //for the database
            var contype = searchQuery.ContractType.ToString().ToLower();
            string fcontractt = SaveFirstLetter(contype); //for the database ( for Contract Type = Training -> contractt = i )

            if (flocation != "WholeCountry")
            {
                fcity = searchQuery.City.ToLower();
            }

            //.....Database initialize.........
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            UserLog dbUser = (from t in userDb.UserLog where t.user_id == userid select t).First();
            JobTitle dbJobtitle = new JobTitle();
            CompanyTitle dbComTitle = new CompanyTitle();
            Location dbLoc = new Location();
            
            //USER TABLE
            if (!userDb.JobTitle.Any(o => o.job_name == fJobtitle))
            {
                dbJobtitle.job_name = fJobtitle;
                userDb.JobTitle.Add(dbJobtitle);
                userDb.SaveChanges();
            }
            dbUser.job_id = (from j in userDb.JobTitle where j.job_name == fJobtitle select j.job_id).First();
            

            if (!userDb.CompanyTitle.Any(o => o.comp_name == fcompName))
            {
                dbComTitle.comp_name = fcompName;
                userDb.CompanyTitle.Add(dbComTitle);
                userDb.SaveChanges();
                
            }
            dbUser.comp_id = (from c in userDb.CompanyTitle where c.comp_name == fcompName select c.comp_id).First();


            if (userDb.ContractType.Any(o => o.contype == fcontractt))
            {
                dbUser.contractt_id = (from ct in userDb.ContractType where ct.contype == fcontractt select ct.contractt_id).First();
            }

            if (userDb.ContractPeriod.Any(o => o.conperiod == fcontractp))
            {
                dbUser.contractp_id = (from cp in userDb.ContractPeriod where cp.conperiod == fcontractp select cp.contractp_id).First();
            }

            userDb.SaveChanges();

            //LOCATION TABLE
            dbLoc.user_id = dbUser.user_id;
            dbLoc.country_id = (from c in userDb.Country where c.country_name == fcountry select c.country_id).First();
            dbLoc.city_name = fcity;

            userDb.Location.Add(dbLoc);
            userDb.SaveChanges();

            //SKILLS TABLE AND USERSKILL TABLE
            
            for (int i = 0; i < fskills.Length; i++)
            {
                Skills dbSkill = new Skills();
                UserSkill dbUserskill = new UserSkill();
                var tmp = fskills[i].ToLower();
                if (!userDb.Skills.Any(o => o.skill_type == tmp))
                {
                    dbSkill.skill_type = tmp;
                    userDb.Skills.Add(dbSkill);
                    userDb.SaveChanges();
                }

                dbUserskill.user_id = dbUser.user_id;
                dbUserskill.skill_id = (from s in userDb.Skills where s.skill_type == tmp select s.skill_id).First();
                userDb.UserSkill.Add(dbUserskill);
                userDb.SaveChanges();
            }
            
            
            t = new Timer(TimerEvent);
            t.Change(5000, Timeout.Infinite); 
            await context.PostAsync("We have successfully created your CV! I will notify you about the available positions for your query! Please wait a bit");
            context.Wait(MessageReceived);
        }

        private async Task AfterJobUpdateForm(IDialogContext context, IAwaitable<UpdateJobTitle> result)
        {
            var job = await result;
            var jobname = job.JobTitle;
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            JobTitle jobDb = new JobTitle();
            if (!userDb.JobTitle.Any(o => o.job_name == jobname))
            {
                jobDb.job_name = jobname.ToLower();
                userDb.JobTitle.Add(jobDb);
                userDb.SaveChanges();
            }

            UserLog jname = (from t in userDb.UserLog where t.user_id == userid select t).First();
            jname.job_id = (from j in userDb.JobTitle where j.job_name == jobname select j.job_id).First();
            userDb.SaveChanges();
            searchQuery.JobTitle = jobname;
            var reply = context.MakeMessage();
            reply.Text = "You have succesfully updated your CV !";
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title ="Continue", Type = ActionTypes.PostBack, Value = "Update"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived);
        }

        private async Task AfterCompUpdateForm(IDialogContext context, IAwaitable<UpdateCompanyName> result)
        {
            var com = await result;
            var comname = com.CompanyName;
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            CompanyTitle comDb = new CompanyTitle();
            if (!userDb.CompanyTitle.Any(o => o.comp_name == comname))
            {
                comDb.comp_name = comname.ToLower();
                userDb.CompanyTitle.Add(comDb);
                userDb.SaveChanges();

            }
            UserLog cname = (from t in userDb.UserLog where t.user_id == userid select t).First();
            cname.comp_id = (from c in userDb.CompanyTitle where c.comp_name == comname select c.comp_id).First();
            userDb.SaveChanges();
            searchQuery.CompanyName = comname;
            var reply = context.MakeMessage();
            reply.Text = "You have succesfully updated your CV !";
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title ="Continue", Type = ActionTypes.PostBack, Value = "Update"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived);

        }

        private async Task AfterLocUpdateForm(IDialogContext context, IAwaitable<UpdateLocation> result)
        {
            var loc = await result;
            string city = null;
            var country = loc.Country.ToString();
            var conop = loc.ChooseLocation.ToString();
            if (conop != "WholeCountry")
            {
                city = loc.City.ToLower();
            }
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();

            Location locdel = (from l in userDb.Location where l.user_id == userid select l).First();
            userDb.Location.Remove(locdel);
            userDb.SaveChanges();
            Location locup = new Location();
            locup.user_id = userid;
            locup.city_name = city;
            locup.country_id = (from c in userDb.Country where c.country_name == country select c.country_id).First();
            userDb.Location.Add(locup);
            userDb.SaveChanges();
            searchQuery.City = city;
            searchQuery.Country = loc.Country;
            var reply = context.MakeMessage();
            reply.Text = "You have succesfully updated your CV !";
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title ="Continue", Type = ActionTypes.PostBack, Value = "Update"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived);

        }

        private async Task AfterHardSUpdateForm(IDialogContext context, IAwaitable<UpdateHardSkills> result)
        {
            var hs = await result;
            var hds = hs.HardSkills;
            string[] hardskills = hds.Split(',').Select(sValue => sValue.Trim()).ToArray();
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            
            for (int i = 0; i < hardskills.Length; i++)
            {
                Skills dbSkill = new Skills();
                UserSkill dbUserSkill = new UserSkill();
                var tmp = hardskills[i].ToLower();
                if (!userDb.Skills.Any(o => o.skill_type == tmp))
                {
                    dbSkill.skill_type = tmp;
                    userDb.Skills.Add(dbSkill);
                    userDb.SaveChanges();
                }

                dbUserSkill.user_id = userid;
                dbUserSkill.skill_id = (from s in userDb.Skills where s.skill_type == tmp select s.skill_id).First();
                userDb.UserSkill.Add(dbUserSkill);
                userDb.SaveChanges();

            }
            searchQuery.HardSkills = searchQuery.HardSkills + "," + hds;
            var reply = context.MakeMessage();
            reply.Text = "You have succesfully updated your CV !";
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title ="Continue", Type = ActionTypes.PostBack, Value = "Update"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived);

        }

        private async Task AfterConUpdateForm(IDialogContext context, IAwaitable<UpdateContract> result)
        {
            var cc = await result;
            var conperiod = cc.ContractPeriod.ToString().ToLower();
            string cperiod = SaveFirstLetter(conperiod);
            var contype = cc.ContractType.ToString().ToLower();
            string ctype = SaveFirstLetter(contype);
            HaRiBoDbEntities userDb = new HaRiBoDbEntities();
            UserLog cup = (from c in userDb.UserLog where c.user_id == userid select c).First();
            cup.contractt_id = (from ct in userDb.ContractType where ct.contype == ctype select ct.contractt_id).First();
            cup.contractp_id = (from cp in userDb.ContractPeriod where cp.conperiod == cperiod select cp.contractp_id).First();
            userDb.SaveChanges();
            searchQuery.ContractPeriod = cc.ContractPeriod;
            searchQuery.ContractType = cc.ContractType;
            var reply = context.MakeMessage();
            reply.Text = "You have succesfully updated your CV !";
            reply.SuggestedActions = new SuggestedActions()
            {

                Actions = new List<CardAction>()
                {
                    new CardAction() {Title ="Continue", Type = ActionTypes.PostBack, Value = "Update"}
                }
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceived);

        }

        //TimerEvent : calls SendResult
        public void TimerEvent(object target)
        {
            t.Dispose();
            SendResult.Resume(userid);
        }

        //SaveFirstLetter: First letter of Contract Type/Period
        public string SaveFirstLetter(string a)
        {
            string b = null;
            if ((a != "all") && (a != "training"))
            {
                b = a[0].ToString();
            }
            else if (a == "training")
            {
                b = "i";
            }
            else
            {
                b = a;
            }
            return b;
        }
        
    }
}