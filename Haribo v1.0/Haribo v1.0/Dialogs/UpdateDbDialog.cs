﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using CareerjetClient;
using Haribo_v1._0.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace Haribo_v1._0.Dialogs
{
    [Serializable]
    public class UpdateDbDialog : IDialog<object>
    {

        private readonly int _userId;

        public UpdateDbDialog(int userid)
        {
            _userId = userid;
        }


        public async Task StartAsync(IDialogContext context)
        {
            
            HaRiBoDbEntities HariboDb = new HaRiBoDbEntities();
            var lastRecord = HariboDb.Country.ToArray().LastOrDefault();
            var lastCountryId = lastRecord.country_id;
            string[] location = new string[3];


            for (int k = 1; k <= lastCountryId; k++) //takes all the countries
            {


                var countryInit = (from Country in HariboDb.Country where Country.country_id == k select Country.country_init).First();

                for (int m = 1; m <= 10; m++) //takes 10 pages from  99 jobs each = 990 jobs in each country
                {

                    Hashtable cargs = new Hashtable();
                    cargs.Add("pagesize", "99");
                    cargs.Add("page", m.ToString());
                    cargs.Add("sort", "date");

                    try
                    {
                        MyClient myClient = new MyClient();
                        string json = myClient.Search(cargs, countryInit);
                        Rootobject job = JsonConvert.DeserializeObject<Rootobject>(json);


                        for (int i = 0; i < job.jobs.Length; i++)
                        {
                            JobLog dbJobLog = new JobLog();
                            dbJobLog.title = job.jobs[i].title;
                            dbJobLog.company = job.jobs[i].company;
                            dbJobLog.puplish_date = job.jobs[i].date;
                            dbJobLog.salary = job.jobs[i].salary;
                            dbJobLog.description = job.jobs[i].description;
                            dbJobLog.url = job.jobs[i].url;
                            location = job.jobs[i].locations.Split(',').Select(sValue => sValue.Trim()).ToArray();
                            dbJobLog.location_city = location[0];
                            if (location.Length > 1)
                            {
                                dbJobLog.location_region = location[1];
                            }
                            dbJobLog.country_id = (from Country in HariboDb.Country
                                                   where Country.country_init == countryInit
                                                   select Country.country_id).First();
                            dbJobLog.update_date = DateTime.UtcNow;
                            HariboDb.JobLog.Add(dbJobLog);
                            HariboDb.SaveChanges();
                        }
                    }
                    catch (SystemException)
                    {
                    } //throws exception in Careerjet.dll

                }

            }


            await context.PostAsync("The database is up to date now. Type \"next\" to resume");

            context.Wait(this.MessageReceivedAsync);


        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            if ((await result).Text == "next")
            {
                await context.PostAsync("Great! whenever you're to see if there is any new job posision available, please type 'retry'.");
                context.Done(String.Empty); //Finish this dialog
            }
            else
            {
                await context.PostAsync("Please type \"next\"");
                context.Wait(MessageReceivedAsync); //Not done yet
            }
        }



    }
}