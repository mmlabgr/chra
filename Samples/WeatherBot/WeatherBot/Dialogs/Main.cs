﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherBot.Dialogs
{
    public class Main
    {
        [JsonProperty(PropertyName = "temp")]
        public string Temperature { get; set; }
        [JsonProperty(PropertyName = "humidity")]
        public string Humidity { get; set; }

    }
}