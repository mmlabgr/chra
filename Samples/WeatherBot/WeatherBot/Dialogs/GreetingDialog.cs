﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace WeatherBot.Dialogs
{
    [Serializable]
    public class GreetingDialog : IDialog
    {
        public static int Count = 0;
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Hi I am Cloud ! Your personal weather bot! ");
              await Respond(context);
            if (Count >= 1)
            {
                context.Done(context);
            }
            else
            {
                context.Wait(MessageReceivedAsync);
            }
            
        }

        private static async Task Respond(IDialogContext context)
        {
            var UserName = String.Empty;
            context.UserData.TryGetValue<string>("Name", out UserName);

            //if it's the first time ask the name 
            if (string.IsNullOrEmpty(UserName))
            {
                await context.PostAsync("What is your name?");
                context.UserData.SetValue<bool>("GetName", true);
            }
            //if the session is still open repeat Welcome greeting
            else
            {
                await context.PostAsync(String.Format("Welcome {0} !", UserName));
                await context.PostAsync("How can I be of service?");
                Count++;
            }
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var UserName = String.Empty;
            var GetName = false;

            context.UserData.TryGetValue<string>("Name", out UserName);
            context.UserData.TryGetValue<bool>("GetName", out GetName);

            if (GetName)
            {
                UserName = message.Text;
                context.UserData.SetValue<string>("Name", UserName);
                context.UserData.SetValue<bool>("GetName", false);

            }

            await Respond(context);
            context.Done(message);
        }
    }
}