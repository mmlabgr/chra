﻿/*using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace WeatherBot.Dialogs
{
    
    [Serializable]
    public class WeatherDialog : LUISDialog
    {
        private static string AppID = "6f10e501a0f3df847ced928a848c7e7d";

        [NonSerialized] private LuisResult Result;
        //private LuisResult Result { get; }

        public WeatherDialog(IDialogContext context, LuisResult result)
        {
            Result = result;
            Weather(context);
        }

        public Task Weather(IDialogContext context)
        {

            //foreach (var entity in Result.Entities.Where(Entity => Entity.Type == "cityname")) // den tha exm pote parapanw apo 1 city opote mporoume na to paraleipsoume 
            //{
            var city = Result.Entities[0].Entity;
            string url = string.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&units=metric&appid={1}", city, AppID);
            using (var httpClient = new HttpClient())
            {
                //await context.PostAsync(string.Format("{0}", city)); den exei thema
                var json = httpClient.GetStringAsync(url); //<-----------ERROR---------
                //deserialize Json 
                WeatherList weather = JsonConvert.DeserializeObject<WeatherList>(json.Result); 
                //await context.PostAsync(string.Format("{0}", weather.main.Temperature));
                 context.PostAsync(string.Format("The temperature in {0} is : {1} °C \n\n" + "Description: {2}", city,weather.main.Temperature, weather.weather[0].Description));
            }

            context.PostAsync("Do you need anything else?");
            //waiting yes or no 
            //context.Wait(Answer);
            context.Done(context);
            //check here !!!!!!!!!!
            //}
        }

        /*       public async Task Answer(IDialogContext context, IAwaitable<IMessageActivity> res)
               {
                   var message = await res;
                   var answer = String.Empty;
                   answer = message.Text.TrimEnd();

                   if (answer == "yes" || answer == "Yes")
                   {
                       await context.PostAsync("Tell me !");
                       context.Done(true); //to exit Answer and wait for new message from user

                   }
                   else if (answer == "no" || answer == "No")
                   {
                       await context.PostAsync("Thank you for using Cloud ! Have a nice day ! ");
                       context.Done(true); //exit Answer and return 
                   }
                   else
                   {
                       await context.PostAsync("Please type \"yes\" or \"no\" ");

                   }
               }
    }
}*/