﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherBot.Dialogs
{
    public class WeatherList
    {
        //weather in JSON is an array so we make it as a list and we call it like: weather[0].NameOfKey
        public List<Weather> weather { get; set; }
        //call it like main.NameOfKey
        public Main main { get; set; }

    }
}