﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;


namespace WeatherBot.Dialogs
{
    [LuisModel("a0f2752e-975b-49aa-873f-49b03c76dd6d", "06cb81e6488f4777bf805ca4f8a0855d")]
    [Serializable]

    public class LUISDialog : LuisDialog<object>
    {
        //Api Id from the openweather url
        private static string AppID = "6f10e501a0f3df847ced928a848c7e7d"; 

        [LuisIntent("")]

        public async Task None(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("I'm sorry, I don't know what you mean!");
            context.Wait(MessageReceived);
        }

        [LuisIntent("Greeting")]

        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            //goes to GreetingDialog and continues with a callback method
            context.Call(new GreetingDialog(), Callback);
        }

        private async Task Callback(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceived);
        }

        [LuisIntent("GetWeather")] 

        public async Task GetWeather(IDialogContext context, LuisResult result)
        {
            
           //context.Call(new WeatherDialog(context, result), Callback);

            foreach (var entity in result.Entities.Where(Entity => Entity.Type == "cityname"))
            {
                //takes value from the response of LUIS
                var city = entity.Entity.ToLower();
                string url = string.Format("http://api.openweathermap.org/data/2.5/weather?q={0}&units=metric&appid={1}", city, AppID);
                using (var httpClient = new HttpClient())
                {
                    var json = await httpClient.GetStringAsync(url);
                    //deserialize Json 
                    WeatherList weather = JsonConvert.DeserializeObject<WeatherList>(json);
                    await context.PostAsync(string.Format("The temperature in {0} is : {1} °C \n\n" + "Description: {2}", city.Replace("_", " "), weather.main.Temperature, weather.weather[0].Description));
                    
                }

                await context.PostAsync("Do you need anything else?");
                //waiting yes or no 
                context.Wait(Answer);
            }

        }
            

       public async Task Answer(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var answer = String.Empty;
            answer = message.Text.TrimEnd();

            if (answer == "yes" || answer == "Yes")
            {
                await context.PostAsync("Tell me !");
                context.Done(true); //to exit Answer and wait for new message from user

            }
            else if (answer == "no" || answer == "No")
            {
                await context.PostAsync("Thank you for using Cloud ! Have a nice day ! ");
                context.Done(true); //exit Answer and return 
            }
            else
            {
                await context.PostAsync("Please type \"yes\" or \"no\" ");
                
            }
        }

        [LuisIntent ("Exit")]

        public async Task Exit(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Bye Bye!");
        }

    }
}