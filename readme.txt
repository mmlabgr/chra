���� ��������� ��� � ������� �������� �� ���������� ��� ���� ������ �� ����� ��� �������� ��� Azure, �� ������ �� ��������������� ������� ��� ���� ������:
1.	Haribo v1.0.sln 
	-���������� �� ������ ��� Visual Studio, ���� Solution Explorer ���� ������� Properties ������� � ������� �Publish Profile�. ��� �� ����� ��� publish, � ������� ������ �� ��������� �� ������������ ������. 
	-��� ��������, ��� ������ Web.config ��� connection string, � ������� ������ �� ������� ��� �������� ����� �� ����� ��� ����������� ���� ����� publish �� bot. 
			
			data source=[server_name];
			user id=[server_admin_login];
			password=[Password];

	-����� ����������� ��� ������� ������ �� ����� publish �� project ����������� �� ���� ��� ���������� Azure.

2.	RetryOnceaweek.sln
	-���������� �� ������ ��� Visual Studio, ���� Solution Explorer ���� ������� Properties ������� � ������� �Publish Profile�. ��� �� ����� ��� publish, � ������� ������ �� ��������� �� ������������ ������.
	-��� ������ Event.cs ������ �� ��������� �� messaging endpoint ��� ��� ������� �� azure ���� ����� publish �� bot ��� �������� ������: 
		HttpRequestMessage reqMsg = new HttpRequestMessage(HttpMethod.Post, messaging_endpoint/api/push");
	-����� ������ �� ��������� publish �� application �� ��� ��������� ���������� Azure.

3.	DailyDbUpdate.sln
	-���������� �� ������ ��� Visual Studio, ���� Solution Explorer ���� ������� Properties ������� � ������� �Publish Profile�. ��� �� ����� ��� publish, � ������� ������ �� ��������� �� ������������ ������, ���� ���� �������� ���������. 
	-�� ������������ Console Application �������� ��� �� ���� ���������, ����� � ������� ������ �� ������� ���� �� connection string ���� ������������ �������� ��� ������ Haribo v1.0.sln. 
	-���� ��� ������������, ������ �� ����� publish ��� project ���� �� �� ��� ��������. 

�� ���� ���������, ��� � ������� ��������������� ������ ������ ��� ���� ���������, �� ������ �� ����� �������� ��� ������� HariboDb.edmx ��� ������ Models. ���� ������������� �������� ���� ���� ��� ������� ��� ������� ���� ���������� �� ������ .edmx. 

����� ��� �� �������� ����� � ���� ��������� ���� ������������� ���� �� ������� ������ �� ����� �� �������� �����:

	-��� ������ 'Contract Type' : 
		id	 contype
		1	 p
		2	 c
		3	 t
		4 	 i
		5	 v
		6	 all

	-��� ������ 'Contract Period' : 
		id 	conperiod
		1  	f
		2  	p
		3  	all

	-��� ������ 'Country' :
		id 	country_init	 country_name	
		1 	el_GR 		 Greece
		2 	en_GB 		 UnitedKingdom
		3 	nl_NL 		 Netherlands 
		��� �������� ����� �� ������ �� ����� ���� ������ �� documentation ��� CareerJet API (https://www.careerjet.com/partners/api/dotnet/doc/Client.html)
